Installing Lithium
==================

Unless you need to install Lithium (which you can put off until you've had a bit of a futher exploration of the docs), you can skip this section and go on to the [Syntax](../syntax/index.md).
