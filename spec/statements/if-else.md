`if` and `else`
===============

The `if` Statement
------------------

As with most other programming languages, Lithium includes an `if` statement. 

```lithium
int x : 1

if( x = 1)
  # This won't run.
  doSomething()
  
# This will run.
doSomethingElse()
```

The `if` statement works by only executing any code after the statement

The `else` Statement
--------------------
