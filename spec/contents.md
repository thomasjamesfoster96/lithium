Contents
========

1.  [__Introduction__](introduction/index.md)
    1. [Hello World!](introduction/hello-world.md)
    2. [Notable Features](introdction/notable-features.md)
    
2.  [__Installing__](installing/index.md)
    1. [The Docs](installing/install-docs.md)
    2. [Lithium-via-Node](installing/lithium-via-node.md)
    3. [Lithium-to-Wiring](installing/lithium-to-wiring.md)

3.  [__Syntax__](syntax/index.md)
    2. [Structure](syntax/structure.md)
    3. [Variables](syntax/variables.md)
    4. [Statements](syntax/statements.md)
    5. [Expressions](syntax/expressions.md)
    6. [Units](syntax/units.md)

4.  [__Variables__](variables/index.md)
    1. [Variable - `var`](variables/variable.md)
    2. [Constants - `cnst`](variables/constant.md)
    3. [Strings - `str`](variables/string.md)
    3. [Integers - `int`](variables/integer.md)
    4. [Decimals - `dec`](variables/decimal.md)
    5. [Booleans - `bool`](variables/boolean.md)
    6. [Lists - `list`](variables/list.md)
    7. [Sets - `set`](variables/sets.md)
    8. [Dictionaries - `dict`](variables/dictionary.md)
    9. [Functions - `fn`](variables/function.md)
    10. [Classes - `class`](variables/function.md)
    11. [Objects - `obj`](variables/objects.md)

5.  [__Statements__](statements/index.md)
    1. [The `if` and `else` statements](statements/if-else.md)
    3. [`while` and `until` statements](statements/while-until.md)
    5. [The `return` statement](statements/return.md)
    6. [`import` statement](statements/import.md)
    7. [The `try` and `catch` statements](statements/try-catch.md)
    9. [`tell` statement](statements/tell.md)

6.  [__Built-in__](built-in/index.md)
    1. [Console - `console`](built-in/console.md) 
    2. [Input/Output - `io`](built-in/io.md)

7.  [__Extending__](extending/index.md)
    1. [Importing modules](extending/import-modules.md)
    2. [Defining modules](extending/create-modules.md)

8.  [__Miscellaneous__](miscellaneous/index.md)
    1. [Documentation](miscellaneous/documentation.md)
