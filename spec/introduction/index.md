Introduction
============

Lithium (yes, it's capitalised!) is sort of like a four-way mash between JavaScript, Python, Ruby and the maths you learnt in high school. Most people should be able to understand it within an hour, assuming they have a bit of experience with using a computer.

The name Lithium comes from, well, the element lithium. Lithium is the lightest metal, yet powers everything from your laptop to your phone to torches to cars. This is sort of the aim with Lithium - create something which is lightweight and can be used almost anywhere.

Lithium's syntax has been crafted with a few ideas in mind, which are below:

* Code is written once but read many times.
* Simple programs should only require a simple understanding to read and write.
* Be feature rich, but not feature heavy.
* Follow standards where practical.
* Everyone's use will be different

While Lithium should be something you can quickly write and use, we are prepared to sacrifice a little bit of speed of coding for speed of understanding later on. Whilst it might be a few seconds quicker to write code one way, it is probably better to sacrifice those few seconds now so that they don't turn into hours later on.

Simple Lithium programs, such as a "Hello World!" example, shouldn't be any longer than they need to be. A Hello World! program only outputs a line of text, so why should the program itself be longer than one line? Keeping Lithium lightweight ultimately makes Lithium code easier to learn, more readable and easier to work with.

Lithium should be feature rich, not feature heavy. This means that there should be lots of creatures to use, but these shouldn't impact someone who doesn't need to use them. They shouldn't make Lithium any slower by being there, nor should someone need to use more features than they should.

Lithium also follows widely accepted standards where possible. The Units feature, for example, only works with SI units. If Lithium works with standards in the worlds of Maths or Science, it should make Lithium much more useful.

Finally, everyone will use Lithium in a different way. Things should be done in a way that suits everyone's needs and requirements.

Next up: [Hello World!](hello-world.md)
