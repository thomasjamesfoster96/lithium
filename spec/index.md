Lithium
=======

This is the Lithium specification, grouped into sections. For a full contents page, see [contents.md](contents.md).

### [Introduction](introduction/index.md)

This is (or rather, will be) a fairly detailed introduction to Lithium. This should get you past the 'hello world' stage of learning Lithium

### [Installing](installing/index.md)

This has some tutorials on installing Lithium via a number of ways.

### [Basic Syntax](syntax/index.md)

A quick overview of the basic syntax.

* Comments
* Variables
* Functions
* Objects and Classes
* Statements
* Block statements

### [Variables](variables/index.md)

A detailed overview of each variable type. Includes type-specific methods and properties.

* Variables
* Constants
* Integers
* Decimals
* Booleans
* Lists
* Sets
* Dictonaries
* Functions
* Classes
* Objects

### [Statements](statements/index.md)

A run through of all the different types of statements, such as `if` and `while`.

* if
* else
* while
* until
