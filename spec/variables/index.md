Variables
=========

Lithium supports almost a dozen data types, and you are welcome to suggest more by [opening an issue](https://github.com/thomasjamesfoster96/lithium/issues/new) with your suggestion. 

### [`var`](variable.md)

A variable that can be of mixed type can be declared as the `var` type.

### [`cnst`](constant.md)

Any variable that won't be changed during a program can be declared as a `cnst`. 

### [`str`](string.md)

The `str` data type is for strings. 

### [`int`](integer.md)

The `int` data type is used for integers.

### [`dec`](decimal.md)

Numbers, such as floating point number, can be declared using the `dec` type. 

### [`bool`](boolean.md)

A boolean value, such as true or false, can be assigned to a variable of type `bool`.

### [`list`](list.md)

A list can be created with the `list` data type.

### [`set`](set.md)

A set can be created with the `set` type.

### [`dict`](dictionary.md)

Using the `dict` data type, a mutable dictionary with `'key' : 'value'` mappings can be created.

### [`fn`](function.md)

A function can be created like any other data type, by using the `fn` type.

### [`class`](class.md)

A class can be declared using the `class` data type.

### [`obj`](object.md)

A plain object can be declared using `obj`.
