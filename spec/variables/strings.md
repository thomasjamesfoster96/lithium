Strings
=======

Strings are one of the main data types supported by Lithium. A string is denoted by the keyword `str` when declaring a variable. Strings are always sourrounded by double quotes.

A string is declared as follows:

```lithium
str myString : "Hello World!"
```

Strings also inhereit methods and properties from the `obj` type.

### Properties

In addition to the properties that each data type inheirits from the `obj` data type, strings also have the following properties:

##### `String.case`



### Methods

### Events
