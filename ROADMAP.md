ROADMAP
=======

This file just contains a list of features for future releases that contributors can help work on. Try and create an issue on GitHub and tag it as a 'feature request' to get it on here. From here, features for the next release are added to the [TODO.md](TODO.md) file, alongside any bug fixes that are needed.

## Version 0.0.0

*   __Types__
   
    `int`, `str`, `var`, `dec`, `list`, `dict`, `obj`, `class`, `set`, `fn`, `const`.

    Lithium 0.0.0 should support the eleven types above. Each type is referred to by a two to five letter abbreviation. `var` is a special type in that it can be of any type. 

*   __Arithmatic__
 
    `+`, `-`, `/`, `×`, `÷`, `√`, etc.

*   __Statements__

    `if`, `else`, `while`, `until`, `for`, etc.

*   __Built-in functions__

    `sin()`, `cos()`, etc.

*   __Built-in constants__

    `π`, `e`, `φ`.

## Version 1.0.0

*   __Docs in HTML__

    As noted in [#1](https://github.com/thomasjamesfoster96/lithium/issues/1), the docs for Lithium should be in, or be easily made available, in HTML.
