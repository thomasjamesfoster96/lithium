FAQ
===

A list of questions about Lithium that have been sarcasticaly answered in a cranky voice.

* * *

### Does Lithium have any special Easter Eggs?

Yes.

### Can you add braces and stuff?

No.

### I don't like Lithium.

That's a statement, not a question.

### What has Lithium been influenced by?

Lots of things.

### Where can I buy some Lithium metal?

Supermarket.

### What can I use Lithium for?

Most things.

### What can't I use Lithium for?

Anything illegal.

### Is Lithium NSA-proof?

Depends on your code.

### Why can't I do X in Lithium?

Because we:
a) Haven't thought of it.
b) Haven't finished adding it yet.
c) Decided it's not feasible.
